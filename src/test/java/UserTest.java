import app.UserController;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;
import pojos.Address;
import pojos.Company;
import pojos.Geo;
import pojos.User;

public class UserTest {

    User user;
    UserController userController = new UserController();


    @Test
    public void testCanCreateUser() throws JsonProcessingException {

        User createdUser = userController.createUser(user);
        ReflectionAssert.assertReflectionEquals(user, createdUser);
    }

    @Test(dependsOnMethods = "testCanCreateUser")
    public void testCanGetUser() {

        User receivedUser = userController.getUser(user.getId());
        ReflectionAssert.assertReflectionEquals(user, receivedUser);
    }

    @Test(dependsOnMethods = "testCanCreateUser")
    public void testCanUpdateUser() throws JsonProcessingException {

        user.setPhone("0987654321");

        User updatedUser = userController.updateUser(user);
        ReflectionAssert.assertReflectionEquals(user, updatedUser);
    }

    @Test(dependsOnMethods = "testCanCreateUser")
    public void canDeleteUser(){
        int code = userController.deleteUser(user.getId());
        Assert.assertEquals(204, code);
    }

    @BeforeClass
    public void createTestUser() {

        Geo geo = new Geo()
                .withLat("12.12345")
                .withLng("12.34567");

        Address address = new Address()
                .withStreet("qwerty street")
                .withSuite("suite")
                .withCity("Dubai")
                .withZipcode("1234567-123")
                .withGeo(geo);

        Company company = new Company()
                .withName("Best")
                .withCatchPhrase("qwerty qwerty qwerty")
                .withBs("zxcvbnm");

        user = new User().withId(1002)
                .withName("Test User 1")
                .withUsername("test_user_name")
                .withEmail("test.user@email.com")
                .withPhone("1234567890")
                .withWebsite("qwerty.qwerty")
                .withAddress(address)
                .withCompany(company);
    }
}
