package app;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pojos.User;

import static com.jayway.restassured.RestAssured.*;

public class UserController {

    private final String url = "https://jsonplaceholder.typicode.com/users";

    ObjectMapper mapper = new ObjectMapper();

    public User createUser(User user) throws JsonProcessingException {

        return with()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(mapper.writeValueAsString(user))
                        .post(url).getBody().as(User.class);
    }

    public User getUser(int id){
        return get(url + "/" + id).getBody().as(User.class);
    }

    public User updateUser(User user) throws JsonProcessingException {

        return with()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(mapper.writeValueAsString(user))
                .put().getBody().as(User.class);
    }

    public int deleteUser(int id){

        return delete(url + "/" + id)
                .then().extract().response().getStatusCode();
    }
}
